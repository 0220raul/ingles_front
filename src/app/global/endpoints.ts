export class Endpoints {
    static readonly apiUrl: string = 'http://192.168.1.4:8080/';

    //Palabras
    static readonly palabras: string = 'palabras/';
    static readonly palabrasSave: string = 'palabras/save';

    //Categorias
    static readonly categorias: string = 'categorias/';
}