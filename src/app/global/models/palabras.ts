import { Categoria } from './categoria';

export class Palabra {
    idPalabra: number;
    ingles: string;
    espanol: string;
    significado: string;
    descripcion: string;
    urlImagen: string;
    fecha: Date;
    categoriaDto: Categoria;
}
