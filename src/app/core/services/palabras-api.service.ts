import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Palabra } from 'src/app/global/models/palabras';
import { Endpoints } from 'src/app/global/endpoints';

@Injectable({
  providedIn: 'root'
})
export class PalabrasApiService {

  constructor(private httpClient: HttpClient) { }

  public palabras() {
    return this.httpClient.get<Palabra[]>(Endpoints.apiUrl + Endpoints.palabras);
  }

  public savePalabra(palabra: Palabra) {
    return this.httpClient.post<Palabra>(Endpoints.apiUrl + Endpoints.palabrasSave, palabra);
  }
}
