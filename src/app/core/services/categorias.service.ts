import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Categoria } from 'src/app/global/models/categoria';
import { Endpoints } from 'src/app/global/endpoints';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  constructor(private httpClient: HttpClient) { }

  public categorias() {
    return this.httpClient.get<Categoria[]>(Endpoints.apiUrl + Endpoints.categorias);
  }

  public saveCategoria(categoria: Categoria) {
    return this.httpClient.post<Categoria>(Endpoints.apiUrl + Endpoints.categorias, categoria);
  }
}
