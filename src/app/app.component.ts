import { Component, OnInit, HostBinding } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'inglesfront';
  @HostBinding("class") classTheme;
  theme: string;

  constructor(private overlayContainer:OverlayContainer) { }

  onSetTheme(theme: string) {
    const overlayContainerElement = this.overlayContainer.getContainerElement().classList;
    const removeElement = Array.from(overlayContainerElement).filter(
      item => item.includes(this.theme)
    );
    if (removeElement.length) {
      overlayContainerElement.remove(...removeElement);
    }
    overlayContainerElement.add(theme);
    this.classTheme = theme;
    this.theme = theme;
    //if (theme.includes('dark')) {
    //  this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#2A3A59';
    //  this.elementRef.nativeElement.ownerDocument.body.style.color = 'white';
    //} else {
    //  this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'white';
    //  this.elementRef.nativeElement.ownerDocument.body.style.color = 'black';
    //}
  }
}
