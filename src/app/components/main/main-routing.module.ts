import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';


const routes: Routes = [
  {
    path: '', component: MainComponent, children: [
      {
        path: 'palabras', loadChildren: () => import('src/app/components/palabras/palabras.module')
          .then(m => m.PalabrasModule),
      },
      {
        path: 'categoria', loadChildren: () => import('src/app/components/categoria/categoria.module')
          .then(m => m.CategoriaModule),
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
