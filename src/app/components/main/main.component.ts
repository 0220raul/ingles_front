import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  navLinks: any[];

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.navLinks = [
      {
        label: 'Palabras',
        link: 'palabras',
        index: 0
      }, {
        label: 'Categorias',
        link: 'categoria',
        index: 1
      }
    ];
  }
}
