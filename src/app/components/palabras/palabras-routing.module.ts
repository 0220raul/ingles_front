import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PalabrasComponent } from './palabras.component';


const routes: Routes = [
  {
    path: '', component: PalabrasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PalabrasRoutingModule { }
