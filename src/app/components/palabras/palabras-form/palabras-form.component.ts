import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Palabra } from 'src/app/global/models/palabras';
import { Categoria } from 'src/app/global/models/categoria';

@Component({
  selector: 'app-palabras-form',
  templateUrl: './palabras-form.component.html',
  styleUrls: ['./palabras-form.component.scss']
})
export class PalabrasFormComponent implements OnInit {

  palabraForm: FormGroup;
  categoriaForm: FormGroup;
  isLinear = true;
  color: ThemePalette = 'accent';
  checked = false;
  disabled = false;

  constructor(private dialogRef: MatDialogRef<PalabrasFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.initForm();
    this.categoriaForm.get('categoria').disable();
  }

  initForm() {
    this.palabraForm = this.fb.group({
      espanol: [null, [Validators.required]],
      ingles: [null, [Validators.required]],
      significado: [null],
      descripcion: [null],
      idCategoria: [[null, [Validators.required]]]
    });
    this.categoriaForm = this.fb.group({
      categoria: [null, [Validators.required]]
    });
  }

  close(isSave: boolean) {
    if (isSave) {
      typeof this.palabraForm.get('idCategoria').value === 'number' || this.categoriaForm.get('categoria').value !== null ?
      this.dialogRef.close(this.getPalabra()):
      this._snackBar.open("Categoria no seleccionada, crea o selecciona una categoria.", "OK", {duration: 3000});
    }else {
      this.dialogRef.close();
    }
  }

  catDisable(event) {
    event.checked && typeof this.palabraForm.get('idCategoria').value !== 'number' ? 
    this.categoriaForm.get('categoria').enable(): this.categoriaForm.get('categoria').disable();
  }

  getPalabra(): Palabra{
    let palabra: Palabra = new Palabra();
    let categoria: Categoria = new Categoria();

    if(typeof this.palabraForm.get('idCategoria').value === 'number') {
      categoria.idCategoria = this.palabraForm.get('idCategoria').value;
      categoria.nombreCategoria 
    } else if(typeof this.palabraForm.get('idCategoria').value !== 'number') {
      categoria.idCategoria = 0;
      categoria.nombreCategoria = this.categoriaForm.get('categoria').value;
    }

    palabra.espanol = this.palabraForm.get('espanol').value;
    palabra.ingles = this.palabraForm.get('ingles').value;
    palabra.significado = this.palabraForm.get('significado').value;
    palabra.descripcion = this.palabraForm.get('descripcion').value;
    palabra.categoriaDto = categoria;
    return palabra;
  }
}
