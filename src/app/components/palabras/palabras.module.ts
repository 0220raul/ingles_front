import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PalabrasRoutingModule } from './palabras-routing.module';
import { PalabrasComponent } from './palabras.component';

import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { PalabrasFormComponent } from './palabras-form/palabras-form.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [PalabrasComponent, PalabrasFormComponent],
  imports: [
    CommonModule,
    PalabrasRoutingModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatSlideToggleModule,
    MatSnackBarModule
  ]
})
export class PalabrasModule { }
