import { Component, OnInit } from '@angular/core';
import { PalabrasApiService } from 'src/app/core/services/palabras-api.service';
import { Palabra } from 'src/app/global/models/palabras';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { PalabrasFormComponent } from 'src/app/components/palabras/palabras-form/palabras-form.component'
import { Categoria } from 'src/app/global/models/categoria';
import { CategoriasService } from 'src/app/core/services/categorias.service';

@Component({
  selector: 'app-palabras',
  templateUrl: './palabras.component.html',
  styleUrls: ['./palabras.component.scss']
})
export class PalabrasComponent implements OnInit {

  palabras$: Observable<Palabra[]>;
  categorias: Observable<Categoria[]>;

  constructor(private palabrasService: PalabrasApiService,
    private categoriasService: CategoriasService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.palabras$ = this.palabrasService.palabras();
    this.categorias = this.categoriasService.categorias();
  }

  openDialog() {
    this.dialog.open(PalabrasFormComponent, {
      width: '800px',
      data: this.categorias,
      disableClose: true
    }).afterClosed().subscribe(data => this.save(data));
  }

  save(data: Palabra) {
    if (data) {
      if (data.categoriaDto.idCategoria !== 0) {
        this.palabrasService.savePalabra(data).subscribe(response => console.log(response));
      } else {
        this.categoriasService.saveCategoria(data.categoriaDto).subscribe(response => {
          data.categoriaDto = response;
          this.palabrasService.savePalabra(data).subscribe();
        });
      }
    }
  }

}
